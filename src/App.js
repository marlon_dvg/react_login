import React, { useState } from "react"
import { Button } from "antd"
import { PlusOutlined, MinusOutlined } from '@ant-design/icons'

import "./App.css"

function App() {
  const [count, setCount] = useState(0);

  function increase() { setCount(count + 1) }
  function decrease() { setCount(count - 1) }

  return (
    <div className="App">
      <h1>{count}</h1>
      <Button icon={<PlusOutlined />} onClick={increase} >Up!</Button><br /><br />
      <Button icon={<PlusOutlined />} onClick={increase} shape="round" type="primary">Up!</Button><br /><br />
      <Button icon={<PlusOutlined />} onClick={increase} type="dashed">Up!</Button><br /><br />

      <Button icon={<MinusOutlined />} onClick={decrease} danger>Down!</Button><br /><br />
      <Button icon={<MinusOutlined />} onClick={decrease} shape="round" type="primary" danger>Down!</Button><br /><br />
      <Button icon={<MinusOutlined />} onClick={decrease} type="dashed" danger>Down!</Button>
    </div>
  )
}

export default App
